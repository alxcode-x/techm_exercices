﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UFundamentals
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            //Exercise 49-1
            #region
            //for(int i = 1; i <= 100; i++)
            //{
            //    if (i % 3 == 0) Console.WriteLine(i);
            //}
            #endregion

            //Exercise 49-2
            #region
            //List<int> numbers = new List<int>();
            //while(true)
            //{
            //    try
            //    {
            //        Console.WriteLine("Enter a number or 'ok' to exit: ");
            //        var number = Console.ReadLine();
            //        if (number != "ok")
            //        {
            //            numbers.Add(Convert.ToInt32(number));
            //        }
            //        else
            //        {
            //            break;
            //        }
            //    }
            //    catch
            //    {
            //        Console.WriteLine("\nUnrecognized character. Please follow the instructions.\n");
            //        continue;
            //    }
            //}

            //foreach(var number in numbers)
            //{
            //    Console.WriteLine(number);
            //}
            #endregion

            //Exercise 49-3
            #region
            //Console.WriteLine("Enter a number: ");
            //int number = Convert.ToInt32(Console.ReadLine());
            //int factorial = number;
            //int i = number;
            //while(i > 1)
            //{
            //    factorial = factorial * --i;
            //}

            //Console.WriteLine($"{number}! = {factorial}");
            #endregion

            //Exercise 49-4
            #region
            //int number = new Random().Next(1,10);
            //Console.WriteLine(number);

            //short i = 1;
            //do
            //{
            //    Console.WriteLine("Enter a number: ");
            //    int guessedNumber = Convert.ToInt32(Console.ReadLine());

            //    if(guessedNumber == number)
            //    {
            //        Console.WriteLine("You Won!");
            //        break;
            //    }else
            //    {
            //        if(i == 4)
            //        {
            //            Console.WriteLine("You Lost!");
            //            break;
            //        }
            //    }
            //    i++;
            //} while (i <= 4);
            #endregion

            //Exercise 49-5
            #region
            //Console.WriteLine("Enter a series of numbers:");
            //string[] serie = Console.ReadLine().Replace(" ", "").Split(',');
            //List<int> numbers = new List<int>();

            //foreach (var item in serie)
            //{
            //    numbers.Add(Convert.ToInt32(item));
            //}

            //numbers = numbers.OrderByDescending(n => n).ToList();
            //Console.WriteLine(numbers[0]);
            #endregion

            //Exercise 56
            #region
            List<string> friendList1 = new List<string>();
            List<string> friendList2 = new List<string>() { "Alexis","Juan" };
            List<string> friendList3 = new List<string>() { "Pepe", "Sara", "Ramon", "Ivan", "Tania" };

            Post post = new Post();
            Console.WriteLine(post.ShowLikes(friendList1));
            Console.WriteLine(post.ShowLikes(friendList2));
            Console.WriteLine(post.ShowLikes(friendList3));
            
            #endregion
        }
    }
}
