﻿using System;
using System.Collections.Generic;
namespace UFundamentals
{
    public class Post
    {
        //private List<string> Friend { get; set; }

        //public Post(List<string> friend)
        //{
        //    this.Friend = friend;
        //}

        public string ShowLikes(List<string> friends)
        {
            return (friends.Count == 0) ? null :
                    (friends.Count <= 2) ? $"{friends[0]} and {friends[1]} like your post" :
                    $"{friends[0]}, {friends[1]} and {friends.Count-2} others like your post";
        }

    }
}
