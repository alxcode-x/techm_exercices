﻿using System;
namespace Person
{
    public class Singer : Person
    {
        public string BandName { get; set; }

        public Singer(string name, string designation, string bandName) : base(name, designation)
        {
            Name = name;
            Designation = designation;
            BandName = bandName;
        }

        public override void Learn()
        {
            Console.WriteLine("Learning new songs");
        }

        public new void Walk()
        {
            Console.WriteLine("Walking in street");
        }

        public sealed override void Eat()
        {
            Console.WriteLine("Eating tacos");
        }

        public void Singing()
        {
            Console.WriteLine("Singing in the bar");
        }

        public void PlayGuitar()
        {
            Console.WriteLine("Playing guitar");
        }

        public override void ShowPerson()
        {
            Console.WriteLine($"\n{Name} - {Designation} - Band: {BandName}");
            Learn();
            Walk();
            Eat();
            Singing();
            PlayGuitar();
        }
    }
}
