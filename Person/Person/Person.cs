﻿using System;
namespace Person
{
    public abstract class Person
    {
        protected string Name { get; set; }
        protected string Designation { get; set; }

        public Person(string name, string designation)
        {
            Name = name;
            Designation = designation;
        }

        public abstract void Learn();

        public void Walk()
        {
            Console.WriteLine("Walking somewhere");
        }

        public abstract void Eat();

        public abstract void ShowPerson();
    }
}
