﻿using System;

namespace Person
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Person programmer = new Programmer("Alexis", "Programmer", "Tech Mahindra");
            Person dancer = new Dancer("Miguel", "Dancer", "The Dancers");
            Person singer = new Singer("Juan", "Singer", "The Singers");
           
            ShowPerson(programmer);
            ShowPerson(dancer);
            ShowPerson(singer);
        }

        public static void ShowPerson(Person person)
        {
            person.ShowPerson();
        }
    }
}
