﻿using System;
namespace Person
{
    public class Dancer : Person
    {
        public string GroupName { get; set; }

        public Dancer(string name, string designation, string groupName) : base(name, designation)
        {
            Name = name;
            Designation = designation;
            GroupName = groupName;
        }

        public override void Learn()
        {
            Console.WriteLine("Learning new types of dance");
        }

        //** Walk() is taking the default behavior of Person **
        //public override void Walk()
        //{
        //    Console.WriteLine("Walking in the park");
        //}

        public override void Eat()
        {
            Console.WriteLine("Eating pizza");
        }

        public static void Dancing()
        {
            Console.WriteLine("Dancing in the theater");
        }

        public override void ShowPerson()
        {
            Console.WriteLine($"\n{Name} - {Designation} - Group: {GroupName}");
            Learn();
            Walk(); // Person
            Eat();
            Dancing();
        }
    }
}
