﻿using System;
namespace Person
{
    public class Programmer : Person
    {
        private string CompanyName { get; set; }

        public Programmer(string name, string designation, string companyName) : base(name, designation)
        {
            Name = name;
            Designation = designation;
            CompanyName = companyName;
        }

        public override void Learn()
        {
            Console.WriteLine("Learning C#");
        }

        public new void Walk()
        {
            Console.WriteLine("Walking in the office");
        }

        public override void Eat()
        {
            Console.WriteLine("Eating burgers");
        }

        public void Coding()
        {
            Console.WriteLine("Coding in .Net 5");
        }

        public override void ShowPerson()
        {
            Console.WriteLine($"\n{Name} - {Designation} - Company: {CompanyName}");
            Learn();
            Walk();
            Eat();
            Coding();
        }
    }
}
