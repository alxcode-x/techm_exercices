﻿using System;
namespace Exercices
{
    public class Calculator
    {
        public float result { get; set; }


        public Calculator()
        {

        }

        public float Calculate(Func<float, float> op)
        {
            return op(result);
        }

        
    }
}
