﻿using System;

namespace Exercices
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            MainClass main = new MainClass();
            Numbers numbers = new Numbers(35);

            Console.WriteLine("Select an option: Calculator[1]  Even/Odd numbers[2]  [3]  [4] ");
            int op = Convert.ToInt16(Console.ReadLine());

            switch (op)
            {
                case 1:
                    main.Calculator();
                    break;
                case 2:
                    numbers.Calculate();
                    break;
                    
            }
        }

        public void Calculator()
        {
            float number1, number2;
            int op;
            Calculator calculator = new Calculator();

            Console.WriteLine("Enter the first number: ");
            number1 = float.Parse(Console.ReadLine());

            Console.WriteLine("Enter the second number: ");
            number2 = float.Parse(Console.ReadLine());

            Console.WriteLine("Select an option: Sum[1]  Sub[2]  Mult[3]  Div[4] ");
            op = Convert.ToInt16(Console.ReadLine());

            switch (op)
            {
                case 1:
                    Console.WriteLine($"{number1} + {number2} = {calculator.Calculate((result) => number1 + number2)}");
                    break;
                case 2:
                    Console.WriteLine($"{number1} + {number2} = {calculator.Calculate((result) => number1 - number2)}");
                    break;
                case 3:
                    Console.WriteLine($"{number1} + {number2} = {calculator.Calculate((result) => number1 * number2)}");
                    break;
                case 4:
                    Console.WriteLine($"{number1} + {number2} = {calculator.Calculate((result) => number1 / number2)}");
                    break;
            }
        }
    }
}
