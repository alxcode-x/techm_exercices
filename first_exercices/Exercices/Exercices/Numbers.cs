﻿using System;
namespace Exercices
{
    public class Numbers
    {
        public int numbers { get; set; }
        int[] numbersArray;

        public Numbers(int numbers)
        {
            this.numbers = numbers;
        }

        public void Calculate()
        {
            FillArray();
            int even = EvenNumbers();
            int odd = OddNumbers();

             Console.WriteLine($"Even numbers: {EvenNumbers()}  |  Odd numbers: {OddNumbers()}");
        }

        private void FillArray()
        {
            numbersArray = new int[numbers];

            for (int i = 0; i < numbers; i++)
            {
                numbersArray[i] = i;
            }
        }


        private int EvenNumbers()
        {
            int sum = 0;

            foreach(var number in numbersArray)
            {
                sum = (number % 2 == 0) ? sum + number : sum;
            }

            return sum;
        }

        private int OddNumbers()
        {
            int sum = 0;

            foreach (var number in numbersArray)
            {
                sum = (number % 2 != 0) ? sum + number : sum;
            }

            return sum;
        }

    }
}
